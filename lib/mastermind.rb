class Code
  attr_reader :pegs
  require 'byebug'

  PEGS = { "R" => "", "G" => "", "B" => "",
           "Y" => "", "O" => "", "P" => "" }.freeze

  def initialize(pegs_arr)
    @pegs = pegs_arr

  end

  def self.parse(code)
    raise "Entered code is not valid" if code.upcase.chars.any? { |c| PEGS[c] == nil }
    Code.new(code.upcase.chars)
  end

  def self.random
    pegs = []
    while pegs.length < 4
      pegs << PEGS.keys[rand * 6]
    end

    Code.new(pegs)
  end

  def exact_matches(guess)
    matches = 0
    self.pegs.each_index { |idx| matches += 1 if self[idx] == guess[idx] }
    matches
  end

  def near_matches(guess)
    matches = 0

    code_pegs = Hash.new(0)
    guess_pegs = Hash.new(0)

    self.pegs.each { |peg| code_pegs[peg] += 1 }
    guess.pegs.each { |peg| guess_pegs[peg] += 1 }

    guess_pegs.each do |peg, count|
      matches += [count, code_pegs[peg]].min
    end

    matches -= exact_matches(guess)
  end

  def [](idx)
    @pegs[idx]
  end

  def ==(guess)
    if guess.is_a?(String)
      return self.pegs == Code.parse(guess)
    else
      return self.pegs == guess.pegs
    end
    false
  end
end

class Game
  attr_reader :secret_code

  def initialize(new_code = Code.random)
    @secret_code = new_code
  end

  def get_guess
    print "Enter your guess: "
    input = gets.chomp
    Code.parse(input)
  end
end

if __FILE__ == $PROGRAM_NAME
  game = Game.new
  guess = game.get_guess

  remaining_guesses = 9

  while game.secret_code != guess && remaining_guesses > 0
    puts "Remaining guesses: #{remaining_guesses}"
    puts "Exact matches: #{game.secret_code.exact_matches(guess)}"
    puts "Near matches: #{game.secret_code.near_matches(guess)}"
    guess = game.get_guess
    remaining_guesses -= 1
  end

  puts "Game over. Answer was #{game.secret_code.pegs}"
end
